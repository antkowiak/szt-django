from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.add_input(Submit('submit', _("Send")))

    contact_name = forms.CharField(required=True, label=_("First name"))
    contact_email = forms.EmailField(required=True, label=_("Email"))
    content = forms.CharField(
        required=True,
        widget=forms.Textarea,
        label=_("Message")
    )
