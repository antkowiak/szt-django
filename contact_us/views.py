from django.shortcuts import render
from .forms import ContactForm
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template import Context
from django.template.loader import get_template
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

# Create your views here.
def contact_us(request):
    form = ContactForm
    if request.method == 'POST':
        form = form(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
                , '')
            contact_email = request.POST.get(
                'contact_email'
                , '')
            form_content = request.POST.get('content', '')

            # Email the profile with the
            # contact information
            template = get_template('contact_us/contact_email.txt')
            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            })
            content = template.render(context)

            email = EmailMessage(
                "Nowa wiadomość",
                content,
                "Your website" + '',
                ['mail@example.org'],
                headers={'Reply-To': contact_email}
            )
            email.send()

            messages.success(request, _('Thanks for contacting us.'))

            return redirect('contact_us')

    return render(request, 'contact_us/index.html', {
        'form': form,
    })
