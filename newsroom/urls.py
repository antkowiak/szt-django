from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<news_id>\d+)/(?P<slug>[\w-]+)$', views.news, name='news'),
    url(r'^$', views.news_list, name='news_list'),
]
