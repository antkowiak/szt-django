from django.shortcuts import render
from django.utils import timezone
from .models import News
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime


def news_list(request):
    queryset_list = News.objects.all().filter(published_date__lte=datetime.today()).order_by('-published_date')

    query = request.GET.get('q')
    if query:
        queryset_list = queryset_list.filter(Q(title__icontains=query) | Q(text__icontains=query))
    else:
        query = ''

    paginator = Paginator(queryset_list, 5)
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)

    get_copy = request.GET.copy()
    parameters = get_copy.pop('page', True) and get_copy.urlencode()

    context = {
        "newsList": queryset,
        "title": "List",
        "query": query,
        "parameters": parameters
    }

    return render(request, 'newsroom/list.html', context)


def news(request, news_id, slug):
    news = News.objects.get(id=news_id)

    return render(request, 'newsroom/show.html', {'news': news})
