from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from ckeditor.fields import RichTextField
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

class News(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    excerpt = models.CharField(max_length=250, default='')
    text = RichTextField()
    image = models.ImageField(upload_to='images', blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def cover(self):
        return '/media/' + self.image.url

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'


@receiver(pre_delete, sender=News)
def news_delete(sender, instance, **kwargs):
    instance.image.delete(False)
