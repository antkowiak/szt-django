from newsroom.models import News
from gallery.models import Gallery, GalleryImage
from django import template

register = template.Library()

@register.simple_tag
def total_news():
    return len(News.objects.all())

@register.simple_tag
def total_galleries():
    return len(Gallery.objects.all())

@register.simple_tag
def total_gallery_imges():
    return len(GalleryImage.objects.all())
