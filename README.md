# Documentation
### Installation
- clone repo
```
git clone git@bitbucket.org:antkowiak/szt-django.git
```
- install docker
- just type
```
docker-compose build
docker-compose up -d
```

### Functionalities
- News CRUD
- Gallery CRUD
- Gallery Images CRUD
- Superadmin / Photographer / Newsman group
- Contact form
- Facebook comments to news and galleries
- Locales support
- News / galleries search
- Gallery grid 3 or 4 remembered in session
- Gallery lightbox
- URL slug
- WYSIWYG news editor
- Session 8h
- Statistics
- Pagination
- Only published on site
- When user is superadmin or has group can edit from site
- Facebook login

### Routes
- /{lang}/admin - Superadmin / Photographer / Newsman dashboard
- /{lang}/accounts/login/ - Login page
- /{lang}/accounts/register/ - Register page
- /accounts/facebook/login - Facebook login
- /{lang}/accounts/register/ - Register page
- /{lang}/accounts/logout/ - Logout
- /{lang}/{news_id}/{slug}/ - View single news
- /{lang}/ - List news
- /{lang}/gallery/{gallery_id}/{slug}/ - View single gallery
- /{lang}/gallery/ - Galleries list
- /{lang}/contact_us/ - Contact us form

### Applications dirs
- cms (project dir include settings and urls definition)
- contact_us (include contact us form)
- docker (docker configuration)
- gallery (include gallery and gallery image: models, admin definitions and views)
- locale (include translations)
- media (uploads dir)
- newsroom (include news: model, admin definitions and views)
- static (static files)
- templates (all html templates used in project)
- templatetags (definitions of filters and tags used in tempaltes)

### Flow:
## Superadmin:
- can add/view/list/delete Users, News, Galleries, GalleryImages and settings also can access dashboard
- can use contact us form
## Photographer:
- can add/view/list/delete Galleries and GalleryImages
- can view/list News
- can use contact us form
## Newsman:
- can add/view/list/delete News
- can view/list Galleries and GalleryImages
- can use contact us form
## Guest (not logged in):
- can view/list News, Galleries and GalleryImages
- can use contact us form

### DB Schema
![Schema](schema.png?raw=true "Schema")
