from django.db import models

# Create your models here.
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from ckeditor.fields import RichTextField
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.utils.safestring import mark_safe
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


class Gallery(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title


class GalleryImage(models.Model):
    gallery = models.ForeignKey(Gallery, default=None)
    image = models.ImageField(upload_to='gallery_images')

    def __str__(self):
        return self.image.url

    def admin_thumbnail(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.image.url))


@receiver(pre_delete, sender=GalleryImage)
def gallery_image_delete(sender, instance, **kwargs):
    if (instance.image):
        instance.image.delete(False)
