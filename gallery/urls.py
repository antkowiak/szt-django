from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^gallery/(?P<gallery_id>\d+)/(?P<slug>[\w-]+)$', views.gallery, name='gallery'),
    url(r'^gallery', views.gallery_list, name='gallery_list'),
]
