from django.contrib import admin
from .models import Gallery, GalleryImage

# Register your models here.

@admin.register(GalleryImage)
class GalleryImageAdmin(admin.ModelAdmin):
    readonly_fields = ('admin_thumbnail',)

class GalleryImageInline(admin.StackedInline):
    model = GalleryImage
    list_display = (
        'image_display'
    )
    # admin_thumbnail = AdminThumbnail(image_field='image')
    readonly_fields = ('admin_thumbnail',)
    fields = ('admin_thumbnail', 'image')


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'published_date'
    )
    inlines = [GalleryImageInline]
