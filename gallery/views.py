from django.shortcuts import render, render_to_response
from django.utils import timezone
from .models import Gallery
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime


# Create your views here.
def gallery_list(request):
    queryset_list = Gallery.objects.all().filter(published_date__lte=datetime.today()).order_by('-published_date')

    query = request.GET.get('q')
    if query:
        queryset_list = queryset_list.filter(Q(title__icontains=query))
    else:
        query = ''

    paginator = Paginator(queryset_list, 5)
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)

    get_copy = request.GET.copy()
    parameters = get_copy.pop('page', True) and get_copy.urlencode()

    context = {
        "galleryList": queryset,
        "title": "List",
        "query": query,
        "parameters": parameters
    }

    return render(request, 'gallery/list.html', context)


def gallery(request, gallery_id, slug):
    gallery = Gallery.objects.get(id=gallery_id)
    grid = 3
    if request.GET.get('grid'):
        grid = int(request.GET.get('grid'))
    elif 'grid' in request.session:
        grid = int(request.session['grid'])
    request.session['grid'] = grid

    return render(request, 'gallery/show.html', {'gallery': gallery, 'grid': grid})
